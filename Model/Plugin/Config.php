<?php

namespace Swiss\ProductSorting\Model\Plugin;

use Magento\Catalog\Model\Config as MagentoConfig;

class Config
{
    /**
     * Adds further sorting-options.
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param array $options
     *
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray(
        MagentoConfig $catalogConfig,
        array $options
    ) {
        $options['category'] = __('Category');
        $options['position'] = __('Relevance');
        return $options;
    }
}
