<?php

namespace Swiss\ProductSorting\Block\Plugin\Product\ProductList;

use Magento\Catalog\Block\Product\ProductList\Toolbar as MagentoToolbar;

class Toolbar extends MagentoToolbar
{
    /**
     * Set collection to pager
     *
     * IMPORTANT
     * Do not put an alias on the table-names and add an empty array after
     * the join-condition.
     * Adding alias risks it crashing and the empty array stops it from
     * fetching extra data, which could interfere with the result.
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @return $this
     */
    public function setCollection($collection)
    {

        $limit             = (int) $this->getLimit();
        $this->_collection = $collection;
        $this->_collection->setCurPage($this->getCurrentPage());

        if ($limit) {
            $this->_collection->setPageSize($limit);
        }

        if ($this->getCurrentOrder()) {
            switch ($this->getCurrentOrder()) {
                case 'category':
                    $joinConditions = [
                        'catalog_category_entity_varchar.entity_id = catalog_category_product.category_id',
                        'catalog_category_entity_varchar.attribute_id = 45',
                    ];

                    $joinConditions = implode(' AND ', $joinConditions);

                    $this->_collection->getSelect()->join(
                        'catalog_category_product',
                        'catalog_category_product.product_id = e.entity_id',
                        []
                    )->join(
                        'catalog_category_entity_varchar',
                        $joinConditions,
                        []
                    )->order(sprintf(
                        'catalog_category_entity_varchar.value %s',
                        $this->getCurrentDirection()
                    ))->group('e.entity_id');

                    break;

                default:
                    $this->_collection->setOrder(
                        $this->getCurrentOrder(), $this->getCurrentDirection()
                    );

                    break;
            }

        }

        return $this;

    }
}
